package com.example.fooddeliveryapp;

public enum Cuisine {
    BREAKFAST("Breakfast"),
    BURGERS("Burgers"),
    STEAK("Steak"),
    SALAD("Salad"),
    DESSERT("Dessert"),
    FISH("Fish");

    Cuisine(String cuisineName) {
        this.cuisineName = cuisineName;
    }

    @Override
    public String toString() {
        return cuisineName;
    }

    // Private properties
    private String cuisineName;
}
