package com.example.fooddeliveryapp;

import java.util.ArrayList;

public class Cart {

    // Singleton
    public static Cart getInstance() {
        if (sharedInstance == null) {
            sharedInstance = new Cart();
        }
        return sharedInstance;
    }
    private static Cart sharedInstance = null;
    private Cart() {} // Don't allow instantiation outside of this class
    // Public properties
    public Integer numberOfItems() {
        Integer numberofItems = 0;
        for (CartItem cartItem : cartItems) {
            numberofItems += cartItem.getQuantity();
       }
        return numberofItems;
    }

    public Integer totalPriceInCents() {
        Integer totalPrice = 0;
        for (CartItem cartItem : cartItems) {
            totalPrice += cartItem.subtotalPriceInCents();
        }
        return totalPrice;
    }

    public void add(Dish dish, Integer quantity) {
        System.out.println("Adding " + quantity + " items of " + dish.name);

        // Check is dish already in our cart?
        CartItem existingCartItem = null;
        for (CartItem cartItem : cartItems) {
            if (cartItem.dish.equals(dish)) {
                existingCartItem = cartItem;
                break;
            }
        }

        // Did we find a matching cart item?
        if (existingCartItem != null) {
            // We found a matching dish in our cart
            existingCartItem.quantity += quantity;
        }
        else {
            // We don't have this dish in our cart yet
            CartItem newCartItem = new CartItem(dish, quantity);
            cartItems.add(newCartItem);
        }
    }

    //Private property
    private ArrayList<CartItem> cartItems = new ArrayList<>();

    public void clear() {
    }

    public ArrayList<CartItem> getCartItems() {
        return null;
    }

    // Nested classes
    public class CartItem {

        // Constructor
        public CartItem(Dish dish, Integer quantity) {
            this.dish = dish;
            this.quantity = quantity;
        }

        // Public methods
        public Integer getQuantity() {
            return quantity;
        }

        public Integer itemPriceInCents() {
            return dish == null ? 0 : dish.priceInCents;
        }

        public String dishName() {
            return dish == null ? "" : dish.name;
        }

        public Integer subtotalPriceInCents() {
            return quantity * itemPriceInCents();
        }

        //Private properties
        private Dish dish;
        private Integer quantity;
    }
}
